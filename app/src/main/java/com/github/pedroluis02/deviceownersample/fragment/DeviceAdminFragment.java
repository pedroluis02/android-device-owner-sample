package com.github.pedroluis02.deviceownersample.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import com.github.pedroluis02.deviceownersample.R;
import com.github.pedroluis02.deviceownersample.receiver.DeviceOwnerReceiver;

public class DeviceAdminFragment extends Fragment {

    private DevicePolicyManager dpm;
    private ComponentName admin;
    private PackageManager packageManager;

    private Switch switchDisableCamera;

    private boolean isBlockSwitches = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_device_admin, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        view.findViewById(R.id.btn_lock_now).setOnClickListener(v -> {
            dpm.lockNow();
        });

        view.findViewById(R.id.btn_wipe_data).setOnClickListener(v -> {
            dpm.wipeData(DevicePolicyManager.WIPE_EXTERNAL_STORAGE);
        });

        switchDisableCamera = view.findViewById(R.id.switch_disable_camera);
        switchDisableCamera.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isBlockSwitches) {
                return;
            }

            dpm.setCameraDisabled(admin, isChecked);
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        showInfo();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        dpm = DeviceOwnerReceiver.getDevicePolicyManager(activity);
        admin = DeviceOwnerReceiver.getComponentName(activity);

        packageManager = getActivity().getPackageManager();
    }

    @Override
    public void onDetach() {
        super.onDetach();

        dpm = null;
        admin = null;
    }

    private void showInfo() {
        isBlockSwitches = true;

        switchDisableCamera.setChecked(dpm.getCameraDisabled(admin));

        isBlockSwitches = false;
    }

}
