package com.github.pedroluis02.deviceownersample;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.github.pedroluis02.deviceownersample.fragment.DeviceAdminFragment;
import com.github.pedroluis02.deviceownersample.fragment.DeviceOwnerFragment;
import com.github.pedroluis02.deviceownersample.fragment.TelephonyInfoFragment;
import com.github.pedroluis02.deviceownersample.preferences.OtherDOFragment;
import com.github.pedroluis02.deviceownersample.preferences.ProvisionInfoFragment;
import com.github.pedroluis02.deviceownersample.receiver.DeviceOwnerReceiver;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";

    private Fragment homeFragment = null;

    private final int REQUEST_ALL_PERMISSIONS = 10;
    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    //private static final String ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";

    private Button btnDeviceOwner;
    private Button btnDeviceAdmin;

    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //checkSystemOverlayPermission();

        init();

        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAppPermissions();
        //}*/

        //test
        //startService(new Intent(this, DOSystemOverlayService.class));
        //startService(new Intent(this, DONotificationListenerService.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAppPermissions();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();

        if (menuItemId == R.id.action_remove_owner) {
            if (DeviceOwnerReceiver.isOwnerActive(this)) {
                Log.i(TAG, "device-owner active");

                try {
                    DevicePolicyManager dpm = DeviceOwnerReceiver.getDevicePolicyManager(this);
                    dpm.clearDeviceOwnerApp(getPackageName());

                    init();
                } catch (Exception e) {
                    Log.e(TAG, "" + e.getMessage());
                }
            } else {
                Log.i(TAG, "device-owner inactive");
            }
        }

        else if (menuItemId == R.id.action_tel_info) {
            setFragment(new TelephonyInfoFragment());
        }

        else if (menuItemId == R.id.action_about) {
            String deviceId = getIdDevice(this);
            Log.i(TAG, "[info] ID: " +  deviceId);

            new AlertDialog.Builder(this)
                    .setTitle(item.getTitle())
                    .setMessage(deviceId + "\n\n"
                            + getString(R.string.app_name) + " v" + BuildConfig.VERSION_NAME
                            + " cod" + BuildConfig.VERSION_CODE)
                    .setPositiveButton("OK", null)
                    .show();
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.i(TAG, "[req-perms] into");
        if (requestCode == REQUEST_ALL_PERMISSIONS) {
            for (int x = 0; x < permissions.length; x++) {
                Log.i(TAG, "[req-perms] " + permissions[x] + " granted=" + (grantResults[x] == PackageManager.PERMISSION_GRANTED));
            }

            //checkSystemOverlayPermission();
        }
    }

    ///
    private void init() {
        btnDeviceOwner = findViewById(R.id.btn_device_owner);
        btnDeviceAdmin = findViewById(R.id.btn_device_admin);

        btnDeviceOwner.setOnClickListener(v -> {
            Log.i(TAG, "onClick devOwner");

            setTabs(0, false);
        });

        btnDeviceAdmin.setOnClickListener(v -> {
            Log.i(TAG, "onClick devAdmin");

            setTabs(1, false);
        });

        findViewById(R.id.btn_other).setOnClickListener((v) -> {
            setTabs(2, false);
        });

        setTabs(0, true);
    }

    // Add fragment to Home
    private void setFragment(Fragment fragment) {
        homeFragment = fragment;
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    // Add Fragment to Back Stack
    private void addFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    private Fragment getCurrentFragment() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            return homeFragment;
        }

        FragmentManager fragmentManager = getFragmentManager();
        for (int x = 0; x < fragmentManager.getBackStackEntryCount(); x++) {
            FragmentManager.BackStackEntry entry = fragmentManager.getBackStackEntryAt(x);
            Fragment fragment = fragmentManager.findFragmentByTag(entry.getName());

            if (fragment != null && fragment.isVisible()) {
                return fragment;
            }
        }

        return null;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private List <String> getDisabledA6Permissions() {
        List<String> list = new ArrayList<>();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_PERMISSIONS);
            String[] requestedPermissions = info.requestedPermissions;

            for (String p : requestedPermissions) {
                if (Manifest.permission.SYSTEM_ALERT_WINDOW.equals(p)) {
                    continue;
                }

                int cs = checkSelfPermission(p);
                Log.i(TAG, "[perms] " + p + " granted=" + (cs == PackageManager.PERMISSION_GRANTED));
                if (cs != PackageManager.PERMISSION_GRANTED) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "[perms] " + e.getMessage());
        }

        return list;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkAppPermissions() {
        if (alertDialog != null && alertDialog.isShowing()) {
            Log.i(TAG, "alertDialog isShowing");
            return;
        }

        List <String> aux1 = getDisabledA6Permissions();
        if (aux1.isEmpty()) {
            ///checkSystemOverlayPermission();
            return;
        }

        alertDialog = new AlertDialog.Builder(this)
                .setTitle("Permissions")
                .setMessage("Permissions disabled.")
                .setCancelable(false)
                .setPositiveButton("Active", (dialog1, which) -> {
                    dialog1.dismiss();

                    alertDialog = null;

                    List <String> list = getDisabledA6Permissions();
                    if (!list.isEmpty()) {
                        String[] reqPermissions = new String[list.size()];
                        list.toArray(reqPermissions);
                        //Log.i(TAG, "[perms] " + Arrays.toString(reqPermissions));

                        requestPermissions(reqPermissions, REQUEST_ALL_PERMISSIONS);
                    } else {
                        //checkSystemOverlayPermission();
                    }
                }).show();
    }

    private void checkSystemOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, REQUEST_ALL_PERMISSIONS);
        } else {
            //checkNotificationServiceListener();
        }
    }

    private void checkNotificationServiceListener() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && DeviceOwnerReceiver.isOwnerActive(this)) {
            boolean result1 = DeviceOwnerReceiver.getDevicePolicyManager(this)
                    .setPermittedCrossProfileNotificationListeners(
                            DeviceOwnerReceiver.getComponentName(this), new ArrayList <String>() {{
                                add(getPackageName());
                            }});
            Log.i(TAG, "[active-nsl] setPermittedCPeNotificationListeners result=" + result1);
            if (result1) {
                return;
            }
        }

        boolean result2 = false;
        String pkgName = getPackageName();
        final String flat = Settings.Secure.getString(getContentResolver(), ENABLED_NOTIFICATION_LISTENERS);
        if (!TextUtils.isEmpty(flat)) {
            final String[] names = flat.split(":");
            for (String name : names) {
                final ComponentName cn = ComponentName.unflattenFromString(name);
                if (cn != null) {
                    if (TextUtils.equals(pkgName, cn.getPackageName())) {
                        result2 = true;
                        break;
                    }
                }
            }
        }

        if (!result2) {
            new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle("Notification access")
                    .setMessage("Enable NotificationListenerService")
                    .setPositiveButton("OK", (dialog, which) -> {
                        startActivity(new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS));
                    }).show();
        }
    }


    private void setTabs(int position, boolean first) {
        if (position == 0) {
            getActionBar().setSubtitle("Device Owner");

            if (!first) {
                Fragment fragment1 = getCurrentFragment();
                if (fragment1 == null || (fragment1 instanceof DeviceOwnerFragment)) {
                    return;
                }
            }

            if (DeviceOwnerReceiver.isOwnerActive(this)) {
                setFragment(new DeviceOwnerFragment());
            } else {
                setFragment(ProvisionInfoFragment.instantiate(ProvisionInfoFragment.AdminType.DEVICE_OWNER));
            }
        } else if (position == 1) {
            getActionBar().setSubtitle("Device Admin");

            if (!first) {
                Fragment fragment2 = getCurrentFragment();
                if (fragment2 == null || (fragment2 instanceof DeviceAdminFragment)) {
                    return;
                }
            }

            if (DeviceOwnerReceiver.isAdminActive(this)) {
                setFragment(new DeviceAdminFragment());
            } else {
                setFragment(ProvisionInfoFragment.instantiate(ProvisionInfoFragment.AdminType.DEVICE_ADMIN));
            }
        } else if (position == 2) {
            getActionBar().setSubtitle("Other");

            if (!first) {
                Fragment fragment3 = getCurrentFragment();
                if (fragment3 == null || (fragment3 instanceof DeviceAdminFragment)) {
                    return;
                }
            }

            //if (DeviceOwnerReceiver.isAdminActive(this)) {
                setFragment(new OtherDOFragment());
            //} else {
            //    setFragment(ProvisionInfoFragment.instantiate(ProvisionInfoFragment.AdminType.DEVICE_ADMIN));
            //}

        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAppPermissions();
        }*/
    }


    public static String getIdDevice(Context contexto) {
        String idDev = "ANDROID-"+
                Build.BRAND.toUpperCase(Locale.ENGLISH)+
                "-"+
                Build.MODEL.toUpperCase(Locale.ENGLISH)+
                Settings.Secure.getString(contexto.getContentResolver(), Settings.Secure.ANDROID_ID).toUpperCase(Locale.ENGLISH);
        if(idDev.length()<=50)
        {
            return idDev;
        }
        return idDev.substring(0,49);
    }

}
