package com.github.pedroluis02.deviceownersample.service;

import android.accessibilityservice.AccessibilityService;
import android.content.Intent;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

public class DOAccessibilityService extends AccessibilityService {

    private final String TAG = "DOAccessibilityService";

    public static final String
            DO_ACTION_HOME = "global_action_home",
            DO_ACTION_RECENTS = "global_action_recent";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.i(TAG, "onAccessibilityEvent");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand action=" + intent.getAction());

        if (DO_ACTION_RECENTS.equals(intent.getAction())) {
            performGlobalAction(GLOBAL_ACTION_RECENTS);
        } else if (DO_ACTION_HOME.equals(intent.getAction())) {
            performGlobalAction(GLOBAL_ACTION_HOME);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.i(TAG, "onServiceConnected");
    }

    @Override
    public void onInterrupt() {
        Log.i(TAG, "onInterrupt");
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
    }

}
