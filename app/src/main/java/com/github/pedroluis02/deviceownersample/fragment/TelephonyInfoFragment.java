package com.github.pedroluis02.deviceownersample.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.github.pedroluis02.deviceownersample.R;

public class TelephonyInfoFragment extends Fragment {


    TextView serialTextView;
    TextView imei1TextView;
    TextView numberTextView;

    TelephonyManager telephonyManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_telephony_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        serialTextView = view.findViewById(R.id.tel_nt_serial);
        String serialStr = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? Build.getSerial() : Build.SERIAL;
        serialTextView.setText(serialStr);


        imei1TextView = view.findViewById(R.id.tel_nt_imei1);
        imei1TextView.setText(telephonyManager.getImei(0));

        numberTextView = view.findViewById(R.id.tel_nt_number);

        numberTextView.setText(telephonyManager.getLine1Number());
    }
}
