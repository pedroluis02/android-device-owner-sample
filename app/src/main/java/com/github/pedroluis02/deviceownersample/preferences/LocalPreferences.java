package com.github.pedroluis02.deviceownersample.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public final class LocalPreferences {

    private static final String DEVICE_ADMIN_PREF_FN = "DeviceAdminPrefs";

    public static final String INSTALL_UNKNOWN_APPS_KEY = "install_unknown_apps",
        HIDDEN_SET_APPS_KEY = "hidden_set_apps",
        SUSPENDED_SET_APPS_KEY = "suspended_set_apps",

        //Kiosk mode
        KIOSK_MODE_ENABLED = "kiosk_mode_enabled", // boolean, actived or deactived
        KIOSK_MODE_APPS = "kiosk_mode_packages", // set <string>
        KIOSK_MODE_RECENT_PACKAGES = "kiosk_mode_history", // set <string>
        KIOSK_MODE_NOTIFICATION_PACKAGES = "kiosk_mode_notifications_packages", // set <string>
        KIOSK_MODE_NOTIFICATION_TITLES = "kiosk_mode_notifications_title", // set <string>

        //white/black list browser chrome
        WHITELIST_URLS = "whitelist_urls", // set <string>
        BLACKLIST_URLS = "blacklist_urls", // set <string>


        // overlay view: last posX and posY
        KIOSK_MODE_OLVIEW_PX = "kiosk_mode_olview_px", // integer
        KIOSK_MODE_OLVIEW_PY = "kiosk_mode_olview_py", // integer
        KIOSK_MODE_OLVIEW_ORIENTATION = "kiosk_mode_olview_orientation" //
    ;

    public static final SharedPreferences getDeviceAdminPreferences(Context context) {
        return context.getSharedPreferences(DEVICE_ADMIN_PREF_FN, Context.MODE_PRIVATE);
    }


    public static int[] getLastOverlayKMPosition(Context context) {
        SharedPreferences pref = getDeviceAdminPreferences(context);

        int x = pref.getInt(KIOSK_MODE_OLVIEW_PX, -1);
        int y = pref.getInt(KIOSK_MODE_OLVIEW_PY, -1);

        if (x < 0 || y < 0) {
            return null;
        }

        return new int[] {
          x, y
        };
    }

    public static void setLastOverlayKMPosition(Context context, int x,  int y) {
        getDeviceAdminPreferences(context).edit()
                .putInt(KIOSK_MODE_OLVIEW_PX, (x < 0) ? 0 : x)
                .putInt(KIOSK_MODE_OLVIEW_PY, (y < 0) ? 0 : y)
                .apply();
    }

}
