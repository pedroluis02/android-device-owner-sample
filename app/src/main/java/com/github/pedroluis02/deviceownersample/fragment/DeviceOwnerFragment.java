package com.github.pedroluis02.deviceownersample.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.UserManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.github.pedroluis02.deviceownersample.AppsKioskActivity;
import com.github.pedroluis02.deviceownersample.BuildConfig;
import com.github.pedroluis02.deviceownersample.DeviceOwnerLockActivity;
import com.github.pedroluis02.deviceownersample.R;
import com.github.pedroluis02.deviceownersample.cosu.DOControlCosu;
import com.github.pedroluis02.deviceownersample.preferences.LocalPreferences;
import com.github.pedroluis02.deviceownersample.receiver.DeviceOwnerReceiver;
import com.github.pedroluis02.deviceownersample.util.DOUtil;
import com.github.pedroluis02.deviceownersample.util.UriUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class DeviceOwnerFragment extends Fragment {

    private final String TAG = "DeviceOwnerFragment";

    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    private final int REQUEST_SYSTEM_OVERLAY_SERVICE = 20,
            REQUEST_NOTIFICATION_ACCESS = 21;

    private boolean isBlockSwitches = false;

    private ComponentName admin;
    private DevicePolicyManager dpm;
    private PackageManager packageManager;
    private SharedPreferences preferences;

    private Switch switchADBMode;
    private Switch switchUnknownSources;

    private final int REQUEST_SELECT_FILE_APK = 10;

    private int typeStartKioskMode = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_device_owner, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        dpm = DeviceOwnerReceiver.getDevicePolicyManager(activity);
        admin = DeviceOwnerReceiver.getComponentName(activity);

        packageManager = getActivity().getPackageManager();
        preferences = LocalPreferences.getDeviceAdminPreferences(getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();

        dpm = null;
        admin = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        showInfo();

        launchAppInCosuMode();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SELECT_FILE_APK && resultCode == Activity.RESULT_OK) {
            installAPK(data);
        } else if (requestCode == REQUEST_SYSTEM_OVERLAY_SERVICE) {
            Log.i(TAG, "[REQUEST_SYSTEM_OVERLAY_SERVICE] rq=" + requestCode);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                startKioskMode();
            } else {
                AlertDialog dialog1 = new AlertDialog.Builder(getActivity())
                        .setCancelable(false)
                        .setTitle("Draw over other apps")
                        .setMessage("Checking permission...")
                        .show();

                new CountDownTimer(/*15000*/5000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        Log.i(TAG, "[CountDownTimer] onTick " + millisUntilFinished);
                        //if (Settings.canDrawOverlays(getActivity())) {
                        //    this.cancel();
                        //}
                    }

                    @Override
                    public void onFinish() {
                        Log.i(TAG, "[CountDownTimer] onFinish");
                        dialog1.dismiss();
                        startKioskMode();
                    }
                }.start();
            }

        } else if (requestCode == REQUEST_NOTIFICATION_ACCESS) {
            Log.i(TAG, "[REQUEST_NOTIFICATION_ACCESS] rq=" + requestCode);
            startKioskMode();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void init(View view) {
        switchADBMode = view.findViewById(R.id.switch_adb_mode);
        switchUnknownSources = view.findViewById(R.id.switch_disable_unknown_sources);

        switchADBMode.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isBlockSwitches) {
                return;
            }
            dpm.setGlobalSetting(admin, Settings.Global.ADB_ENABLED, isChecked ? "1" : "0");
        });

        switchUnknownSources.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isBlockSwitches) {
                return;
            }
            SharedPreferences pref1 = LocalPreferences.getDeviceAdminPreferences(getActivity());
            if (isChecked) {
                dpm = DeviceOwnerReceiver.getDevicePolicyManager(getActivity());
                dpm.addUserRestriction(admin, UserManager.DISALLOW_INSTALL_UNKNOWN_SOURCES);
            } else {
                dpm.clearUserRestriction(admin, UserManager.DISALLOW_INSTALL_UNKNOWN_SOURCES);
            }

            pref1.edit()
                    .putBoolean(LocalPreferences.INSTALL_UNKNOWN_APPS_KEY, isChecked)
                    .apply();
        });

        // block uninstall app
        view.findViewById(R.id.btn_block_uninstall_app).setOnClickListener(v -> {
            blockUninstall(((Button) v).getText().toString(), true);
        });

        view.findViewById(R.id.btn_unblock_uninstall_app).setOnClickListener(v -> {
            blockUninstall(((Button) v).getText().toString(), false);
        });

        // hide app
        view.findViewById(R.id.btn_hide_app_1).setOnClickListener((v) -> {
            hideApplication("Hide Application", true);
        });

        view.findViewById(R.id.btn_hide_app_2).setOnClickListener((v) -> {
            hideApplication("Show Hidden Applications", false);
        });

        // suspend app
        view.findViewById(R.id.btn_suspend_app_1).setOnClickListener((v) -> {
            suspendApps("Suspend Application", true);
        });

        view.findViewById(R.id.btn_suspend_app_2).setOnClickListener((v) -> {
            suspendApps("Enable Suspended Applications", false);
        });

        // reboot (Android 7 o higher)
        view.findViewById(R.id.btn_reboot).setOnClickListener((v) -> {
            rebootDevice();
        });

        view.findViewById(R.id.btn_set_do_lockInfo).setOnClickListener((v) -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                dpm.setDeviceOwnerLockScreenInfo(admin, "This phone belongs to <UserName>");
            }
        });

        // install apk
        view.findViewById(R.id.btn_install_apk).setOnClickListener(v -> {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                Toast.makeText(getActivity(), "Available on Android 6 or higher", Toast.LENGTH_LONG)
                        .show();
            }

            // Select APK file
            startActivityForResult(new Intent(Intent.ACTION_GET_CONTENT)
                    .setType("application/vnd.android.package-archive"),
                    REQUEST_SELECT_FILE_APK);
        });

        // uninstall app
        view.findViewById(R.id.btn_uninstall_app).setOnClickListener(v -> {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                Toast.makeText(getActivity(), "Available on Android 6 or higher", Toast.LENGTH_LONG)
                        .show();
            }

            uninstallApp(((Button) v).getText().toString());
        });

        // lock screen with activity (with taskLock)
        view.findViewById(R.id.btn_lock_device).setOnClickListener((v) -> {
            lockDevice();
        });

        //COSU: dedicated device for app
        view.findViewById(R.id.btn_start_cosu).setOnClickListener((v) -> {
            startCosu();
        });

        view.findViewById(R.id.btn_start_kiosk_mode).setOnClickListener((v) -> {
            typeStartKioskMode = 0;
            startKioskMode();
        });

        view.findViewById(R.id.btn_start_kiosk_mode_2).setOnClickListener((v) -> {
            typeStartKioskMode = 1;
            startKioskMode();
        });
    }

    private void showInfo() {
        isBlockSwitches = true;

        SharedPreferences pref1 = LocalPreferences.getDeviceAdminPreferences(getActivity());

        switchADBMode.setChecked(getBooleanGlobalSetting(Settings.Global.ADB_ENABLED));
        switchUnknownSources.setChecked(pref1.getBoolean(LocalPreferences.INSTALL_UNKNOWN_APPS_KEY, false));

        isBlockSwitches = false;
    }

    private boolean checkDrawOverOtherAppPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(getActivity())) {
            return true;
        }

        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {//USING APP OPS MANAGER
            AppOpsManager manager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            if (manager != null) {
                try {
                    int result = manager.checkOp(AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW, Binder.getCallingUid(), context.getPackageName());
                    if (result == AppOpsManager.MODE_ALLOWED) {
                        return true;
                    }
                } catch (Exception ignore) {
                }
            }
        }*/

        try {//IF This Fails, we definitely can't do it
            WindowManager mgr = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            if (mgr == null) {
                return false; //getSystemService might return null
            }

            View viewToAdd = new View(getActivity());
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(0, 0, Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ?
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY : WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSPARENT);
            viewToAdd.setLayoutParams(params);
            mgr.addView(viewToAdd, params);
            mgr.removeView(viewToAdd);

            return true;
        } catch (Exception e) {
            Log.i(TAG, "[WindowManager] ex: " + e.getMessage());
        }

        return false;
    }

    private boolean checkNotificationServiceListener() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && DeviceOwnerReceiver.isOwnerActive(getContext())) {
            boolean result1 = DeviceOwnerReceiver.getDevicePolicyManager(getContext())
                    .setPermittedCrossProfileNotificationListeners(
                            DeviceOwnerReceiver.getComponentName(getContext()), new ArrayList <String>() {{
                                add(getActivity().getPackageName());
                            }});
            Log.i(TAG, "[active-nsl] setPermittedCPeNotificationListeners result=" + result1);
            if (result1) {
                return true;
            }
        }

        boolean result2 = false;
        String pkgName = getActivity().getPackageName();
        final String flat = Settings.Secure.getString(getActivity().getContentResolver(), ENABLED_NOTIFICATION_LISTENERS);
        if (!TextUtils.isEmpty(flat)) {
            final String[] names = flat.split(":");
            for (String name : names) {
                final ComponentName cn = ComponentName.unflattenFromString(name);
                if (cn != null) {
                    if (TextUtils.equals(pkgName, cn.getPackageName())) {
                        result2 = true;
                        break;
                    }
                }
            }
        }

        if (result2) {
            return true;
        }

        return false;
    }

    private boolean getBooleanGlobalSetting(String setting) {
        return 0 != Settings.Global.getInt(getActivity().getContentResolver(), setting, 0);
    }

    private void blockUninstall(String title, final boolean block) {
        List <AuxApp> list = new ArrayList<>();

        // Only user apps
        List <PackageInfo> packages = packageManager.getInstalledPackages(0);
        for (PackageInfo pkgInfo : packages) {
            if (!getActivity().getPackageName().equals(pkgInfo.packageName)
                    && !DOUtil.isSystemPackage(pkgInfo.applicationInfo)) {
                if (block && !dpm.isUninstallBlocked(admin, pkgInfo.packageName)) {
                    list.add(new AuxApp(pkgInfo.packageName, pkgInfo.applicationInfo.loadLabel(packageManager).toString()));
                } else if (!block && dpm.isUninstallBlocked(admin, pkgInfo.packageName)) {
                    list.add(new AuxApp(pkgInfo.packageName, pkgInfo.applicationInfo.loadLabel(packageManager).toString()));
                }
            }
        }

        createAppsAlertDialog(title, list, (auxApp) -> {
            dpm.setUninstallBlocked(admin, auxApp.packageName, block);

            String msg;
            boolean result = dpm.isUninstallBlocked(admin, auxApp.packageName);
            if (block) {
                msg = "block-uninstall=" + result;
            } else {
                msg = "unblock-uninstall=" + !result;
            }

            msg = msg + " pkg=" + auxApp.packageName;
            Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
        });
    }

    private void hideApplication(String title, final boolean hide) {
        if (!hide) {
            Set <String> hiddenApps = preferences.getStringSet(LocalPreferences.HIDDEN_SET_APPS_KEY, Collections.emptySet());

            String[] result = new String[hiddenApps.size()];
            int x = 0;

            Set <String> forRemove = new HashSet<>();
            for (String pkg : hiddenApps) {
                boolean r = dpm.setApplicationHidden(admin, pkg, false);

                Log.i(TAG, "[unhide] pkg=" + pkg + " unhide=" + r);

                r = dpm.isApplicationHidden(admin, pkg);
                result[x] = pkg + " unhide=" + (!r);
                if (!r) {
                    forRemove.add(pkg);
                }

                x++;
            }

            for (String pkg : forRemove) {
                hiddenApps.remove(pkg);
            }

            preferences.edit()
                    .putStringSet(LocalPreferences.HIDDEN_SET_APPS_KEY, hiddenApps)
                    .apply();

            if (result.length > 0) {
                new AlertDialog.Builder(getActivity())
                        .setTitle(title)
                        .setItems(result, null)
                        .setPositiveButton("OK", null)
                        .show();
            } else {
                Log.i(TAG, "[unhide] empty apps");
                Toast.makeText(getActivity(), "Empty hidden apps", Toast.LENGTH_LONG)
                        .show();
            }

            return;
        }


        List <AuxApp> list = new ArrayList<>();

        List <PackageInfo> packages = packageManager.getInstalledPackages(0);
        for (PackageInfo pkgInfo : packages) {
            if (!getActivity().getPackageName().equals(pkgInfo.packageName)) {
                list.add(new AuxApp(pkgInfo.packageName, pkgInfo.applicationInfo.loadLabel(packageManager).toString()));
            }
        }

        createAppsAlertDialog(title, list, (auxApp) -> {
            boolean result = dpm.setApplicationHidden(admin, auxApp.packageName, true);

            Log.i(TAG, "[hide] pkg=" + auxApp.packageName + " hide=" + result);
            Toast.makeText(getActivity(),  "hide=" + result + " pkg=" + auxApp.packageName, Toast.LENGTH_LONG)
                    .show();

            result = dpm.isApplicationHidden(admin, auxApp.packageName);
            if (result) {
                Set <String> happs = preferences.getStringSet(LocalPreferences.HIDDEN_SET_APPS_KEY, new HashSet<>());
                happs.add(auxApp.packageName);

                preferences.edit()
                        .putStringSet(LocalPreferences.HIDDEN_SET_APPS_KEY, happs)
                        .apply();
            }
        });
    }

    private void suspendApps(String title, final boolean suspend) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Toast.makeText(getActivity(), "Available on Android 7 or higher", Toast.LENGTH_LONG)
                    .show();
        } else {
            if (!suspend) {
                Set <String> aux = preferences.getStringSet(LocalPreferences.SUSPENDED_SET_APPS_KEY, new HashSet<>());
                if (!aux.isEmpty()) {
                    String[] items = new String[aux.size()];
                    aux.toArray(items);

                    Set <String> forRemove = new HashSet<>();
                    dpm.setPackagesSuspended(admin, items, false);
                    int x = 0;
                    for (String s : aux) {
                        try {
                            boolean result = !dpm.isPackageSuspended(admin, s);
                            items[x] = s + " enabled=" + result;
                            if (result) {
                                forRemove.add(s);
                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            forRemove.add(s);
                        }
                        x++;
                    }

                    for (String s : forRemove) {
                        aux.remove(s);
                    }

                    preferences.edit()
                            .putStringSet(LocalPreferences.SUSPENDED_SET_APPS_KEY, aux)
                            .apply();

                    new AlertDialog.Builder(getActivity())
                            .setTitle(title)
                            .setItems(items, null)
                            .setPositiveButton("OK", null)
                            .show();
                } else {
                    Log.i(TAG, "[suspend] empty apps");
                    Toast.makeText(getActivity(), "Empty suspended apps", Toast.LENGTH_LONG)
                            .show();
                }

                return;
            }

            createAppsAlertDialog(title, getUserApps(), auxApp -> {
                String[] result = dpm.setPackagesSuspended(admin, new String[]{
                        auxApp.packageName
                }, true);

                Log.i(TAG, "[suspend] setPackagesSuspended  res-len=" + result.length);

                Set <String> aux = preferences.getStringSet(LocalPreferences.SUSPENDED_SET_APPS_KEY, new HashSet<>());
                try {
                    boolean res = dpm.isPackageSuspended(admin, auxApp.packageName);
                    if (res) {
                        aux.add(auxApp.packageName);
                    }

                    Toast.makeText(getActivity(), "suspended=" + res + " pkg=" + auxApp.packageName, Toast.LENGTH_LONG)
                            .show();
                } catch (PackageManager.NameNotFoundException e) {}

                preferences.edit()
                        .putStringSet(LocalPreferences.SUSPENDED_SET_APPS_KEY, aux)
                        .apply();
            });
        }
    }

    private void installAPK(Intent data) {
        try {
            Log.i(TAG, data.toString());
            String apkPath = UriUtil.getFilePath(getActivity(), data.getData());
            Log.i(TAG, "path=" + apkPath);

            PackageInfo pkg2 = getActivity().getPackageManager().getPackageArchiveInfo(apkPath, 0);
            if (pkg2 != null) {
                Log.i(TAG, String.format("pkg=%s version=%s", pkg2.packageName, pkg2.versionName));
                boolean t2 = DOUtil.installPackage(getActivity(), pkg2.packageName, new File(apkPath));
                Log.d(TAG, "installing result=" + t2);
                Toast.makeText(getActivity(), "installing=" + t2 + " pkg=" + pkg2.packageName, Toast.LENGTH_LONG).show();
            } else {
                throw new Exception("Apk pkg failed file=" + apkPath);
            }
        } catch (Exception e) {
            Log.e(TAG, "[install] " + e.getMessage());
            Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void uninstallApp(String title) {
        List <AuxApp> list = new ArrayList<>();

        // Only user apps
        List <PackageInfo> packages = packageManager.getInstalledPackages(0);
        for (PackageInfo pkg : packages) {
            if (!getActivity().getPackageName().equals(pkg.packageName)
                    && !DOUtil.isSystemPackage(pkg.applicationInfo)) {
                list.add(new AuxApp(pkg.packageName, pkg.applicationInfo.loadLabel(packageManager).toString()));
            }
        }

        createAppsAlertDialog(title, list, (auxApp) -> {
            boolean result = DOUtil.uninstallPackage(getActivity(), auxApp.packageName);
            Log.i(TAG, "uninstalling result=" + result);
            Toast.makeText(getActivity(), "uninstalling=" + result + " pkg=" + auxApp.packageName, Toast.LENGTH_LONG).show();
        });
    }

    private void rebootDevice() {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                Toast.makeText(getActivity(), "Available on Android 7 or higher", Toast.LENGTH_LONG)
                        .show();
            } else {
                dpm.reboot(admin);
            }
        } catch (Exception e) {
            Log.e(TAG, "[reboot] " + e.getMessage());
        }
    }

    private void startCosu() {
        final DOControlCosu controlCosu = new DOControlCosu(getActivity());
        if (controlCosu.isInLockTaskMode()) {
            Toast.makeText(getActivity(), "LockTaskMode started!", Toast.LENGTH_LONG).show();
            return;
        }

        controlCosu.setSelectAppCosuListener((packageName) -> {
            if (!controlCosu.isInLockTaskMode()) {

                Intent launch = packageManager.getLaunchIntentForPackage(packageName);

                Log.i(TAG, "COSU pkg=" + DOControlCosu.DEFAULT_PACKAGE_NAME);

                controlCosu.setDefaultCosuPolicies(true);
                getActivity().startLockTask();

                launch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(launch);

                // Timer
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        DOControlCosu controlDCosu = new DOControlCosu(getActivity());
                        if (controlDCosu.isInLockTaskMode()) {
                            Log.i(TAG, "COSU stop pkg=" + DOControlCosu.DEFAULT_PACKAGE_NAME);

                            controlDCosu.setDefaultCosuPolicies(false);
                            getActivity().stopLockTask();
                        }
                    }
                }, 30_000); // 30 seg
            }
        });
        controlCosu.showInstalledPackageList();
    }

    private void lockDevice() {
        DOControlCosu controlCosu = new DOControlCosu(getActivity());
        if (!controlCosu.isInLockTaskMode()) {
            getActivity().startActivity(new Intent(getActivity(), DeviceOwnerLockActivity.class));
            getActivity().finish();
        } else {
            Toast.makeText(getActivity(), "LockTaskMode started!", Toast.LENGTH_LONG).show();
        }
    }

    private void launchAppInCosuMode() {
        DOControlCosu controlCosu = new DOControlCosu(getActivity());
        if (controlCosu.isInLockTaskMode()) {
            Log.i(TAG, "re-launch pkg=" + DOControlCosu.DEFAULT_PACKAGE_NAME);

            Intent launch = getActivity().getPackageManager().getLaunchIntentForPackage(DOControlCosu.DEFAULT_PACKAGE_NAME);
            if(launch != null) {
                launch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(launch);
            }
        }
    }

    private void startKioskMode() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            Toast.makeText(getActivity(), "Available on Android 6 or higher", Toast.LENGTH_LONG)
                    .show();
        }

        if (checkDrawOverOtherAppPermission()) {
            Log.i(TAG, "[checkDrawOverOtherAppPermission] true");
            if (!checkNotificationServiceListener()) {
                Log.i(TAG, "active notification access");
                Toast.makeText(getActivity(), "Active \"Notification access\"'", Toast.LENGTH_LONG)
                        .show();

                Intent intent = new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
                startActivityForResult(intent, REQUEST_NOTIFICATION_ACCESS);
                return;
            }
        } else {
            Log.i(TAG, "active draw overlay");
            Toast.makeText(getActivity(), "Active \"Draw over other apps\"'", Toast.LENGTH_LONG)
                    .show();

            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getActivity().getPackageName()));
            startActivityForResult(intent, REQUEST_SYSTEM_OVERLAY_SERVICE);
            //startActivity(intent);

            return;
        }

        DOControlCosu controlCosu = new DOControlCosu(getActivity());
        if (controlCosu.isInLockTaskMode()) {
            Toast.makeText(getActivity(), "LockTaskMode started!", Toast.LENGTH_LONG).show();
            return;
        }

        if (typeStartKioskMode == 0) {
            List<AuxApp> items = getAppsWithLauncher();
            String[] labels = new String[items.size()];
            String[] names = new String[items.size()];

            for (int x = 0; x < items.size(); x++) {
                names[x] = items.get(x).packageName;
                labels[x] = (items.get(x).label /*+ "\n" + names[x]*/);
            }

            Set<String> selectionList = new HashSet<>();

            new AlertDialog.Builder(getActivity())
                    .setTitle("Select Apps for Kiosk")
                    .setCancelable(false)
                    .setMultiChoiceItems(labels, null, (dialog, position, isChecked) -> {
                        if (isChecked) {
                            selectionList.add(names[position]);
                        } else {
                            selectionList.remove(names[position]);
                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .setPositiveButton("Accept", (dialog, which) -> {
                        if (selectionList.isEmpty()) {
                            Toast.makeText(getActivity(), "Empty list, select some app.", Toast.LENGTH_LONG).show();
                            return;
                        }

                        dialog.dismiss();

                        preferences.edit()
                                .putStringSet(LocalPreferences.KIOSK_MODE_APPS, selectionList)
                                .putStringSet(LocalPreferences.KIOSK_MODE_RECENT_PACKAGES, new HashSet<>())
                                .apply();

                        getActivity().startActivity(new Intent(getActivity(), AppsKioskActivity.class));
                        getActivity().finish();
                    })
                    .show();
        } else if (typeStartKioskMode == 1) {
            preferences.edit()
                    .putStringSet(LocalPreferences.KIOSK_MODE_APPS, new HashSet<>(getAllLauncherApps()))
                    .putStringSet(LocalPreferences.KIOSK_MODE_RECENT_PACKAGES, new HashSet<>())
                    .apply();

            getActivity().startActivity(new Intent(getActivity(), AppsKioskActivity.class));
            getActivity().finish();
        }

        typeStartKioskMode = -1;
    }

    private List <AuxApp> getAppsWithLauncher() {
        List <AuxApp> launchers = new ArrayList<>();

        List <ApplicationInfo> apps = packageManager.getInstalledApplications(0);
        for (ApplicationInfo info : apps) {
            if (!BuildConfig.APPLICATION_ID.equals(info.packageName) && packageManager.getLaunchIntentForPackage(info.packageName) != null) {
                AuxApp aux = new AuxApp();
                aux.packageName = info.packageName;
                aux.label = info.loadLabel(packageManager).toString();

                launchers.add(aux);
            }
        }

        return launchers;
    }

    private List <AuxApp> getUserApps() {
        List <AuxApp> launchers = new ArrayList<>();

        List <PackageInfo> apps = packageManager.getInstalledPackages(0);
        for (PackageInfo info : apps) {
            if (!BuildConfig.APPLICATION_ID.equals(info.packageName) && !DOUtil.isSystemPackage(info.applicationInfo)) {
                AuxApp aux = new AuxApp();
                aux.packageName = info.packageName;
                aux.label = info.applicationInfo.loadLabel(packageManager).toString();

                launchers.add(aux);
            }
        }

        return launchers;
    }

    private void createAppsAlertDialog(String title, List <AuxApp> items, OnAuxAppListener listener) {
        AuxAppAdapter adapter = new AuxAppAdapter(getActivity(), items);
        new AlertDialog.Builder(getActivity())
                .setTitle(TextUtils.isEmpty(title) ? "List of Apps" : title)
                .setAdapter(adapter, (dialog1, position) -> {
                    dialog1.dismiss();

                    if (listener != null) {
                        listener.onSelectedAuxApp(adapter.getItem(position));
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();

    }

    private class AuxApp {

        private String packageName;
        private String label;

        private AuxApp() {
        }

        private AuxApp(String packageName, String label) {
            this.packageName = packageName;
            this.label = TextUtils.isEmpty(label) ? "<Unknown>" : label;
        }

    }

    @FunctionalInterface
    private interface OnAuxAppListener {

        void onSelectedAuxApp(AuxApp auxApp);

    }

    private class AuxAppAdapter extends ArrayAdapter <AuxApp> {

        private AuxAppAdapter(Context context, List <AuxApp> items) {
            super(context, android.R.layout.simple_list_item_2, android.R.id.text1, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);

            TextView view1 = view.findViewById(android.R.id.text1);
            TextView view2 = view.findViewById(android.R.id.text2);

            AuxApp info = getItem(position);
            view1.setText(info.label);
            view2.setText(info.packageName);

            return view;
        }

    }

    // Test kiosk-mode //
    private List <String> getAllLauncherApps() {
        List<String> list = new ArrayList<>();
        for (ApplicationInfo info : getActivity().getPackageManager().getInstalledApplications(0)) {
            if ((getActivity().getPackageName().compareToIgnoreCase(info.packageName) != 0) && getActivity().
                    getPackageManager().getLaunchIntentForPackage(info.packageName) != null) {
                list.add(info.packageName);
            }
        }

        return list;
    }

    private List<String> getpackagesTest1() {
        return new ArrayList <String>() {{
            add("com.google.zxing.client.android");
            add("com.android.chrome");
            add("com.google.android.calculator");
            add("com.estrongs.android.pop");
            add("com.skype.raider");
        }};
    }

}
