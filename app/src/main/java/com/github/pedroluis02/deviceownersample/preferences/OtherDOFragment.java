package com.github.pedroluis02.deviceownersample.preferences;

import android.app.Activity;
import android.app.Fragment;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.RestrictionEntry;
import android.content.RestrictionsManager;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.github.pedroluis02.deviceownersample.R;
import com.github.pedroluis02.deviceownersample.receiver.DeviceOwnerReceiver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OtherDOFragment extends Fragment {

    private final String TAG = "DeviceOwnerFragment";

    private ComponentName admin;
    private DevicePolicyManager dpm;
    private PackageManager packageManager;
    private RestrictionsManager restrictionsManager;

    private Spinner spinnerAppps;
    private ListView listViewRestrictions;

    private ArrayAdapter <SpinnerApp> appsAdapter;
    private ArrayAdapter <RestrictionEntry> restrictionsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_other, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinnerAppps = view.findViewById(R.id.spinnerItems);
        listViewRestrictions = view.findViewById(R.id.listViewItems);

        appsAdapter = new ArrayAdapter<>(spinnerAppps.getContext(), android.R.layout.simple_list_item_1);
        restrictionsAdapter = new ArrayAdapter<>(listViewRestrictions.getContext(), android.R.layout.simple_list_item_1);

        spinnerAppps.setAdapter(appsAdapter);
        listViewRestrictions.setAdapter(restrictionsAdapter);

        spinnerAppps.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerApp app = (SpinnerApp) parent.getItemAtPosition(position);
                loadManifestAppRestrictions(app.packageName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        loadLaunchableApps();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        dpm = DeviceOwnerReceiver.getDevicePolicyManager(activity);
        admin = DeviceOwnerReceiver.getComponentName(activity);

        packageManager = getActivity().getPackageManager();
        restrictionsManager = (RestrictionsManager) getActivity().getSystemService(Context.RESTRICTIONS_SERVICE);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        dpm = null;
        admin = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        int position = spinnerAppps.getSelectedItemPosition();
        if (position > 0) {
            loadManifestAppRestrictions(appsAdapter.getItem(position).packageName);
        }
    }

    private void loadLaunchableApps() {
        List <SpinnerApp> list = new ArrayList<>();
        for (ApplicationInfo info : packageManager.getInstalledApplications(0)) {
            Intent launcher = packageManager.getLaunchIntentForPackage(info.packageName);
            if (launcher != null) {
                list.add(new SpinnerApp(info.packageName, info.loadLabel(packageManager).toString()));
            }
        }

        appsAdapter.clear();
        appsAdapter.add(new SpinnerApp(null, "Select App..."));
        appsAdapter.addAll(list);
    }

    private void loadManifestAppRestrictions(String packageName) {
        restrictionsAdapter.clear();
        if (packageName == null) {
            return;
        }

        List<RestrictionEntry> manifestRestrictions = null;
        try {
            manifestRestrictions = restrictionsManager.getManifestRestrictions(packageName);
            convertTypeChoiceAndNullToString(manifestRestrictions);
        } catch (NullPointerException e) {
            // This means no default restrictions.
        }

        if (manifestRestrictions != null) {
            restrictionsAdapter.addAll(Arrays.asList(manifestRestrictions.toArray(new RestrictionEntry[0])));
        }
    }

    private void convertTypeChoiceAndNullToString(List<RestrictionEntry> restrictionEntries) {
        for (RestrictionEntry entry : restrictionEntries) {
            if (entry.getType() == RestrictionEntry.TYPE_CHOICE ||
                    entry.getType() == RestrictionEntry.TYPE_NULL) {
                entry.setType(RestrictionEntry.TYPE_STRING);
            }
        }
    }

    private class SpinnerApp {

        private String label;
        private String packageName;

        private SpinnerApp(String packageName, String label) {
            this.packageName = packageName;
            this.label = label;
        }

        @Override
        public String toString() {
            return TextUtils.isEmpty(label) ? packageName : label;
        }

    }

}
