package com.github.pedroluis02.deviceownersample.cosu;

import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.UserManager;

import android.app.AlertDialog;
import android.util.Log;
import android.widget.ArrayAdapter;


import com.github.pedroluis02.deviceownersample.DeviceOwnerLockActivity;
import com.github.pedroluis02.deviceownersample.receiver.DeviceOwnerReceiver;

import java.util.List;

/**
 * set owner: http://florent-dupont.blogspot.pe/2015/01/android-shell-command-dpm-device-policy.html
 * unpin: adb shell input keyevent --longpress 4 187
 * https://codelabs.developers.google.com/codelabs/cosu/index.html?index=..%2F..%2Findex#8
 * https://developers.google.com/android/work/
 *
 * Android for Work: Single Use Devices : https://www.youtube.com/watch?v=j3QC6hcpy90
 * https://www.youtube.com/watch?v=39NkpWkaH8M&index=2&list=PLOU2XLYxmsIKAK2Bhv19H2THwF-22O5WX
 *
 * http://www.sureshjoshi.com/mobile/android-kiosk-mode-without-root/
 * http://florent-dupont.blogspot.pe/2015/02/10-things-to-know-about-device-owner.html
 * https://developer.android.com/about/versions/android-5.0.html#Enterprise
 *
 */

public class DOControlCosu {

    private final String TAG = "DOControlCosu";

    public interface ISelAppCosuListener {
        void onSelectedAppCosu(String packageName);
    }

    private Context mContext;
    private DevicePolicyManager mDevicePolicyManager;
    private ComponentName mAdminComponentName;
    private PackageManager mPackageManager;

    private class SelAppCosu {
        public String packageName = "";
        public String label = "";

        @Override
        public String toString() {
            return (label == null || label.isEmpty()) ? packageName : label;
        }
    }

    private ArrayAdapter <SelAppCosu> appAdapter;

    private static final String Battery_PLUGGED_ANY = Integer.toString(
            BatteryManager.BATTERY_PLUGGED_AC |
                    BatteryManager.BATTERY_PLUGGED_USB |
                    BatteryManager.BATTERY_PLUGGED_WIRELESS);

    private static final String DONT_STAY_ON = "0";

    public static /*final*/ String DEFAULT_PACKAGE_NAME = "";
    private static /*final*/ String DEFAULT_PACKAGE_ACTIVITY = "";

    private ISelAppCosuListener mListener;

    private boolean isLocalActivity = false;

    public DOControlCosu(Context context) {
        mContext = context;

        mAdminComponentName = DeviceOwnerReceiver.getComponentName(mContext);
        mDevicePolicyManager = DeviceOwnerReceiver.getDevicePolicyManager(context);
        mPackageManager = context.getPackageManager();
    }

    public DOControlCosu setSelectAppCosuListener (ISelAppCosuListener listener) {
        mListener = listener;

        return this;
    }

    public void showInstalledPackageList() {
        PackageManager packageManager = mContext.getPackageManager();
        List <ApplicationInfo> appList = packageManager.getInstalledApplications(0);

        appAdapter = new ArrayAdapter<> (mContext, android.R.layout.simple_list_item_1);
        for (ApplicationInfo app : appList) {
            SelAppCosu  item = new SelAppCosu();
            item.packageName = app.packageName;
            item.label = app.loadLabel(packageManager).toString();

            //Log.d(TAG, "app: " + item.toString());

            if (packageManager.getLaunchIntentForPackage(app.packageName) != null) {
                appAdapter.add(item);
            }
        }

        new AlertDialog.Builder(mContext)
                .setTitle("Select App for COSU")
                .setAdapter(appAdapter, (dialog1, index) -> {
                    dialog1.dismiss();

                    SelAppCosu item = appAdapter.getItem(index);
                    DEFAULT_PACKAGE_NAME = item.packageName;
                    DEFAULT_PACKAGE_ACTIVITY = item.packageName;

                    Log.d(TAG, "app: " + item.toString() + " p: " + item.packageName);
                    if (mListener != null) {
                        mListener.onSelectedAppCosu(item.packageName);
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

    public void showLockDevice() {
        DEFAULT_PACKAGE_NAME = mContext.getPackageName();
        DEFAULT_PACKAGE_ACTIVITY = DeviceOwnerLockActivity.class.getName();

        isLocalActivity = true;
    }

    public boolean isInLockTaskMode() {
        // start lock task mode if it's not already active
        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);

        // ActivityManager.getLockTaskModeState api is not available in pre-M.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (!am.isInLockTaskMode()) {
                return false;
            }
        } else {
            if (am.getLockTaskModeState() == ActivityManager.LOCK_TASK_MODE_NONE) {
                return false;
            }
        }

        return true;
    }

    public void setDefaultCosuPolicies(boolean active) {
        /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }*/

        // set user restrictions
        setUserRestriction(UserManager.DISALLOW_SAFE_BOOT, active);
        setUserRestriction(UserManager.DISALLOW_FACTORY_RESET, active);
        setUserRestriction(UserManager.DISALLOW_ADD_USER, active);
        setUserRestriction(UserManager.DISALLOW_MOUNT_PHYSICAL_MEDIA, active);
        setUserRestriction(UserManager.DISALLOW_ADJUST_VOLUME, active);

        //api 23
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // disable keyguard and status bar
            mDevicePolicyManager.setKeyguardDisabled(mAdminComponentName, active);
            mDevicePolicyManager.setStatusBarDisabled(mAdminComponentName, active);

            // set System Update policy
            /*if (active){
                mDevicePolicyManager.setSystemUpdatePolicy(mAdminComponentName,
                    SystemUpdatePolicy.createWindowedInstallPolicy(60, 120));
            } else {
                mDevicePolicyManager.setSystemUpdatePolicy(mAdminComponentName, null);
            }*/
        }

        // enable STAY_ON_WHILE_PLUGGED_IN
        enableStayOnWhilePluggedIn(active);

        // set this Activity as a lock task package

        /*mDevicePolicyManager.setLockTaskPackages(mAdminComponentName,
                active ? new String[]{getPackageName()} : new String[]{});*/
        mDevicePolicyManager.setLockTaskPackages(mAdminComponentName, new String[] {
                DEFAULT_PACKAGE_NAME,
                mContext.getPackageName()
        });

        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_MAIN);
        intentFilter.addCategory(Intent.CATEGORY_HOME);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        intentFilter.addCategory(Intent.CATEGORY_LAUNCHER);

        if (active) {
            // set Cosu activity as home intent receiver so that it is started
            // on reboot
            /*mDevicePolicyManager.addPersistentPreferredActivity(
                    mAdminComponentName, intentFilter, new ComponentName(
                            getPackageName(), CosuActivity.class.getName()));*/

            ComponentName componentActivity = new ComponentName(DEFAULT_PACKAGE_NAME, DEFAULT_PACKAGE_ACTIVITY);
            mDevicePolicyManager.addPersistentPreferredActivity(mAdminComponentName, intentFilter, componentActivity);

            if (!isLocalActivity) {
                mDevicePolicyManager.addPersistentPreferredActivity(mAdminComponentName, intentFilter,
                        new ComponentName(mContext.getPackageName(), mContext.getPackageName()));
            }
        } else {
            /*mDevicePolicyManager.clearPackagePersistentPreferredActivities(
                    mAdminComponentName, getPackageName());*/
            mDevicePolicyManager.clearPackagePersistentPreferredActivities(mAdminComponentName,
                    DEFAULT_PACKAGE_NAME);
            if (!isLocalActivity) {
                mDevicePolicyManager.clearPackagePersistentPreferredActivities(mAdminComponentName, mContext.getPackageName());
            }
        }

        isLocalActivity = false;
    }

    private void setUserRestriction(String restriction, boolean disallow) {
        /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }*/

        if (disallow) {
            mDevicePolicyManager.addUserRestriction(mAdminComponentName, restriction);
        } else {
            mDevicePolicyManager.clearUserRestriction(mAdminComponentName, restriction);
        }
    }

    private void enableStayOnWhilePluggedIn(boolean enabled) {
        /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (enabled) {
            mDevicePolicyManager.setGlobalSetting(
                    mAdminComponentName,
                    Settings.Global.STAY_ON_WHILE_PLUGGED_IN,
                    Battery_PLUGGED_ANY);
        } else {
            mDevicePolicyManager.setGlobalSetting(
                    mAdminComponentName,
                    Settings.Global.STAY_ON_WHILE_PLUGGED_IN, DONT_STAY_ON);
        }*/
    }

}
