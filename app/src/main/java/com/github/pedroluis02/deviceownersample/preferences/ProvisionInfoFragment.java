package com.github.pedroluis02.deviceownersample.preferences;

import android.app.Activity;
import android.app.Fragment;
import android.app.admin.DevicePolicyManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.github.pedroluis02.deviceownersample.R;
import com.github.pedroluis02.deviceownersample.receiver.DeviceOwnerReceiver;

public class ProvisionInfoFragment extends Fragment {

    public static final String EXTRA_ADMIN_TYPE = "admin_type";

    public enum AdminType {

        UNKNOWN(-1),

        DEVICE_ADMIN(0),
        DEVICE_OWNER(1);

        private int value;
        AdminType(int option) {
            value = option;
        }

    }

    private AdminType adminType = AdminType.UNKNOWN;
    private Button btnActiveAdmin;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_provision_info, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnActiveAdmin = view.findViewById(R.id.action_active_md);
        if (AdminType.DEVICE_ADMIN.value == adminType.value) {
            btnActiveAdmin.setVisibility(View.VISIBLE);

            btnActiveAdmin.setOnClickListener((v) -> {
                activeDeviceAdmin();
            });
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        Bundle bundle = getArguments();
        adminType.value = bundle.getInt(EXTRA_ADMIN_TYPE);
    }

    private void activeDeviceAdmin() {
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
                .putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, DeviceOwnerReceiver.getComponentName(getActivity()))
                .putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Enable Device Administration for this application.");

        startActivity(intent);
    }

    public static ProvisionInfoFragment instantiate(AdminType type) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ADMIN_TYPE, type.value);

        ProvisionInfoFragment fragment = new ProvisionInfoFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

}
