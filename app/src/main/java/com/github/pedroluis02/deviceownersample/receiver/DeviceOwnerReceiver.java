package com.github.pedroluis02.deviceownersample.receiver;

import android.annotation.TargetApi;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.PersistableBundle;
import android.util.Log;

import com.github.pedroluis02.deviceownersample.MainActivity;

public class DeviceOwnerReceiver extends DeviceAdminReceiver {

    private final String TAG = "DeviceOwnerReceiver";

    private final String EXTRA_PROFILE_NAME = "profile_name";

    // Device admin / owner
    @Override
    public void onEnabled(Context context, Intent intent) {
        Log.i(TAG, "onEnabled");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && isOwnerActive(context)) {
            //enablePermissionsWithOwner(context);
            getDevicePolicyManager(context)
                    .setPermissionPolicy(getComponentName(context), DevicePolicyManager.PERMISSION_POLICY_AUTO_GRANT);
        }

        launchMainActivity(context);
    }

    // Device admin
    @Override
    public void onDisabled(Context context, Intent intent) {
        Log.i(TAG, "onDisabled");

        launchMainActivity(context);
    }

    @Override
    public void onProfileProvisioningComplete(Context context, Intent intent) {
        Log.i(TAG, "onProfileProvisioningComplete");

        // Set profile name
        DevicePolicyManager dpm = getDevicePolicyManager(context);
        String profileName = "";
        try {
            PersistableBundle extras = intent.getParcelableExtra(DevicePolicyManager.EXTRA_PROVISIONING_ADMIN_EXTRAS_BUNDLE);
            profileName = extras.getString(EXTRA_PROFILE_NAME, "");
        } catch (Exception ignored) { }

        dpm.setProfileName(getComponentName(context), profileName.isEmpty() ? "Unknown" : profileName);
    }

    @Override
    public void onLockTaskModeEntering(Context context, Intent intent, String pkg) {
        Log.i(TAG, "onLockTaskModeEntering");
    }

    @Override
    public void onLockTaskModeExiting(Context context, Intent intent) {
        Log.i(TAG, "onLockTaskModeExiting");

        launchMainActivity(context);
    }

    /* Methods */

    private void launchMainActivity(Context context) {
        context.startActivity(new Intent(context, MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void enablePermissionsWithOwner(Context context) {
        Log.i(TAG, "enablePermissionsWithOwner");
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS);
            DevicePolicyManager dpm = getDevicePolicyManager(context);
            for (String permission : info.requestedPermissions) {
                dpm.setPermissionGrantState(getComponentName(context), context.getPackageName(),
                        permission,
                        DevicePolicyManager.PERMISSION_GRANT_STATE_GRANTED);
            }
        } catch (Exception ignored) {}
    }

    /*  Static*/

    public static ComponentName getComponentName(Context context) {
        return new ComponentName(context.getApplicationContext(), DeviceOwnerReceiver.class);
    }

    public static DevicePolicyManager getDevicePolicyManager(Context context) {
        DevicePolicyManager dpm = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        return dpm;
    }

    // Device Admin
    public static boolean isAdminActive(Context context) {
        DevicePolicyManager dpm = getDevicePolicyManager(context);
        return dpm.isAdminActive(getComponentName(context));
    }

    // Device Owner
    public static boolean isOwnerActive(Context context) {
        DevicePolicyManager dpm = getDevicePolicyManager(context);
        return dpm.isDeviceOwnerApp(context.getPackageName());
    }

}
