package com.github.pedroluis02.deviceownersample.service;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.github.pedroluis02.deviceownersample.AppsKioskActivity;
import com.github.pedroluis02.deviceownersample.BuildConfig;
import com.github.pedroluis02.deviceownersample.preferences.LocalPreferences;
import com.github.pedroluis02.deviceownersample.R;
import com.github.pedroluis02.deviceownersample.util.DOUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//Ref: https://gist.github.com/bjoernQ/6975256

public class DOSystemOverlayService extends /*Service*/ NotificationListenerService
        implements View.OnTouchListener {

    private final String TAG = "DOSystemOverlayService";

    public static final String ACTION_SHOW_MAIN_BAR = (BuildConfig.APPLICATION_ID + ".ACTION_SHOW_MAIN_BAR"),
            ACTION_HIDE_MAIN_BAR = (BuildConfig.APPLICATION_ID + ".ACTION_HIDE_BUTTONS_BAR");

    private WindowManager windowManager;
    private LayoutInflater layoutInflater;

    /* KioskMode toolbar */
    private LinearLayout buttonsBarView;
    private View topLeftView;
    //private CounterFab counterFabNoty;

    private View notificationsView;
    private Drawable collapsedDrawable;
    private Drawable expandedDrawable;

    private float offsetX = 0;
    private float offsetY = 0;
    private int originalXPos = 0;
    private int originalYPos = 0;
    private boolean moving;

    private boolean blockClicked1 = false;
    private boolean blockOnNotificationRemoved = false;

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate");
        setTheme(R.style.AppTheme);

        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        int layout_flag;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            layout_flag = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            layout_flag = WindowManager.LayoutParams.TYPE_PRIORITY_PHONE;
            //layout_flag = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        }

        WindowManager.LayoutParams buttonBarParams = new WindowManager.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                layout_flag,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        //| WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN //new
                        | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.UNKNOWN);

        buttonBarParams.gravity = Gravity.LEFT | Gravity.TOP;
        //buttonBarParams.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        //buttonBarParams.x = 0;
        //buttonBarParams.y = 0;
        Point size = new Point();
        int[] pos = LocalPreferences.getLastOverlayKMPosition(this);
        if (pos == null) {
            windowManager.getDefaultDisplay().getSize(size);
            buttonBarParams.x = (size.x / 2) - DOUtil.dpToPx(100);
            buttonBarParams.y = size.y;
        } else {
            buttonBarParams.x = pos[0];
            buttonBarParams.y = pos[1];
        }

        buttonsBarView = (LinearLayout) layoutInflater.inflate(R.layout.layout_do_overlay, null);
        buttonsBarView.setVisibility(View.GONE);
        /*@SuppressLint("RestrictedApi")
        ContextThemeWrapper ctx = new ContextThemeWrapper(this, R.style.Theme_AppCompat);
        buttonsBarView= (LinearLayout) LayoutInflater.from(ctx).inflate(R.layout.layout_do_overlay, null);*/

        int overLayoutOrientation = LocalPreferences.getDeviceAdminPreferences(this)
                .getInt(LocalPreferences.KIOSK_MODE_OLVIEW_ORIENTATION, 0);
        buttonsBarView.setOrientation((overLayoutOrientation == 0) ? LinearLayout.HORIZONTAL : LinearLayout.VERTICAL);

        buttonsBarView.findViewById(R.id.btn_home).setOnClickListener(v -> {
            Log.i(TAG, "[btn-home] onclick");

            startActivity(new Intent(this, AppsKioskActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        });

        buttonsBarView.findViewById(R.id.btn_recent).setOnClickListener(v -> {
            Log.i(TAG, "[btn-recent] onclick");

            showRecentApps();
        });

        //counterFabNoty = buttonsBarView.findViewById(R.id.btn_notifications);
        //counterFabNoty.setCount(0);
        buttonsBarView.findViewById(R.id.btn_notifications).setOnClickListener(v -> {
            Log.i(TAG, "[btn-notify] onclick");
            showActiveNotifications();
        });

        buttonsBarView.findViewById(R.id.btn_move).setOnTouchListener(this);
        buttonsBarView.findViewById(R.id.btn_move).setOnClickListener(v -> {
            showOverlayOrientation();
        });

        ///
        topLeftView = new View(this);
        WindowManager.LayoutParams topLeftParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                layout_flag,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        //| WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN //new
                        | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.UNKNOWN);
        topLeftParams.gravity = Gravity.LEFT | Gravity.TOP;
        //topLeftParams.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        topLeftParams.x = 0;
        topLeftParams.y = 0;
        topLeftParams.width = 0;
        topLeftParams.height = 0;

        /* Notifications Layout */
        notificationsView = layoutInflater.inflate(R.layout.notifications_layout, null);
        notificationsView.setVisibility(View.GONE);
        notificationsView.findViewById(R.id.nt_action_close).setOnClickListener(v -> {
            //Hide notifications view
            notificationsView.setVisibility(View.GONE);

            //Show static buttons
            if (buttonsBarView != null) {
                buttonsBarView.setVisibility(View.VISIBLE);
            }
        });

        notificationsView.findViewById(R.id.nt_action_clear_all).setOnClickListener(v -> {
            Log.i(TAG, "[clear-all] onClick");

            try {
                blockOnNotificationRemoved = true;
                cancelAllNotifications();
                blockOnNotificationRemoved = false;

                //Hide notifications view
                notificationsView.setVisibility(View.GONE);

                //Show static buttons
                if (buttonsBarView != null) {
                    buttonsBarView.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                Log.e(TAG, "[clear-all] ex:" + e.getMessage());
            }
        });

        WindowManager.LayoutParams notificationsParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                layout_flag,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        //| WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN //new
                        | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSPARENT);
        notificationsParams.gravity = Gravity.LEFT | Gravity.TOP;
        notificationsParams.x = 0;
        notificationsParams.y = 0;

        //add
        windowManager.addView(notificationsView, notificationsParams);
        windowManager.addView(buttonsBarView, buttonBarParams);
        windowManager.addView(topLeftView, topLeftParams);

        //drawables
        collapsedDrawable = DOUtil.getVectorDrawable(this, R.drawable.ic_keyboard_arrow_down);
        expandedDrawable = DOUtil.getVectorDrawable(this, R.drawable.ic_keyboard_arrow_up);
    }


    @Override
    public IBinder onBind(Intent intent) {
        //return null;
        return super.onBind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = (intent != null) ? intent.getAction() : "";
        Log.i(TAG, "[service-action] action=" + action);
        if (ACTION_SHOW_MAIN_BAR.equals(action)) {
            if (buttonsBarView != null && buttonsBarView.getVisibility() == View.GONE) {
                Log.i(TAG, "[buttonsBar] visible=true");
                buttonsBarView.setVisibility(View.VISIBLE);
            }

            if (notificationsView != null && notificationsView.getVisibility() == View.VISIBLE) {
                Log.i(TAG, "[notificationsView] visible=false");
                notificationsView.setVisibility(View.GONE);
            }
        } else if (ACTION_HIDE_MAIN_BAR.equals(action)) {
            if (buttonsBarView != null && buttonsBarView.getVisibility() == View.VISIBLE) {
                Log.i(TAG, "[buttonsBar] visible=false");
                buttonsBarView.setVisibility(View.GONE);
            }

            if (notificationsView != null && notificationsView.getVisibility() == View.VISIBLE) {
                Log.i(TAG, "[notificationsView] visible=false");
                notificationsView.setVisibility(View.GONE);
            }
        }

        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy");

        windowManager.removeView(notificationsView);
        windowManager.removeView(buttonsBarView);
        windowManager.removeView(topLeftView);

        notificationsView = null;
        buttonsBarView = null;
        topLeftView = null;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            float x = event.getRawX();
            float y = event.getRawY();

            moving = false;

            int[] location = new int[2];
            buttonsBarView.getLocationOnScreen(location);

            originalXPos = location[0];
            originalYPos = location[1];

            offsetX = originalXPos - x;
            offsetY = originalYPos - y;

            Log.i(TAG, String.format("DOWN: evX=%f evY=%f, orX=%d orY=%d", x, y, originalXPos, originalYPos));
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            int[] topLeftLocationOnScreen = new int[2];
            topLeftView.getLocationOnScreen(topLeftLocationOnScreen);

            Log.i(TAG, String.format("MOVE: ovlX=%d ovlY=%d", topLeftLocationOnScreen[0], topLeftLocationOnScreen[1]));

            float x = event.getRawX();
            float y = event.getRawY();

            WindowManager.LayoutParams params = (WindowManager.LayoutParams) buttonsBarView.getLayoutParams();

            int newX = (int) (offsetX + x);
            int newY = (int) (offsetY + y);

            Log.i(TAG, String.format("MOVE: evX=%f evY=%f, newX=%d newY=%d, orX=%d orY=%d", x, y, newX, newY, originalXPos, originalYPos));

            if (Math.abs(newX - originalXPos) < 1 && Math.abs(newY - originalYPos) < 1 && !moving) {
                return false;
            }

            params.x = newX - (topLeftLocationOnScreen[0]);
            params.y = newY - (topLeftLocationOnScreen[1]);

            windowManager.updateViewLayout(buttonsBarView, params);
            moving = true;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            WindowManager.LayoutParams params = (WindowManager.LayoutParams) buttonsBarView.getLayoutParams();
            Log.i(TAG, String.format("UP: newX=%d newY=%d", params.x, params.y));
            //LocalPreferences.setLastOverlayKMPosition(this, params.x, params.y);


            /*Point size = new Point();
            windowManager.getDefaultDisplay().getSize(size);

            int px = (params.x < 0) ? 0 : params.x;
            int py = (params.y < 0) ? 0 : params.y;
            boolean apVer = false;
            if (buttonsBarView.getOrientation() == LinearLayout.HORIZONTAL && (py + buttonsBarView.getWidth()) <= size.y) {
                if ((px <= buttonsBarView.getWidth()/2)) {
                    buttonsBarView.setOrientation(LinearLayout.VERTICAL);
                    apVer = true;
                } else if ((px + buttonsBarView.getWidth() >= (size.x - buttonsBarView.getWidth()/2))) {
                    buttonsBarView.setOrientation(LinearLayout.VERTICAL);
                    params.x = px + buttonsBarView.getWidth()/2;
                    windowManager.updateViewLayout(buttonsBarView, params);
                    apVer = true;
                }
            }

            if (!apVer && buttonsBarView.getOrientation() == LinearLayout.VERTICAL && (py + buttonsBarView.getWidth() <= size.y)) {
                buttonsBarView.setOrientation(LinearLayout.HORIZONTAL);
            }*/

            if (moving) {
                moving = false;

                Log.i(TAG, String.format("UP: save newX=%d newY=%d", params.x, params.y));
                LocalPreferences.setLastOverlayKMPosition(this, params.x, params.y);

                return true;
            }
        }

        return false;
    }

    private void showRecentApps() {
        SharedPreferences preferences = LocalPreferences.getDeviceAdminPreferences(this);
        PackageManager mPackageManager = getPackageManager();

        Set<String> recentPackages = preferences.getStringSet(LocalPreferences.KIOSK_MODE_RECENT_PACKAGES, new HashSet<>());
        /*Set <String> recentPackages = new HashSet<String>(){{
            add("com.android.chrome");
            add("com.google.android.youtube");
            add("com.google.android.apps.docs");
            add("com.google.android.gm");
        }};*/

        List <String[]> apps = new ArrayList<>();
        for (String packageName : recentPackages) {
            try {
                ApplicationInfo info = mPackageManager.getApplicationInfo(packageName, 0);
                apps.add(new String[] {
                        packageName,
                        mPackageManager.getApplicationLabel(info).toString()
                });
            } catch (Exception e) {
                Log.i(TAG, "[recent:error] " + e.getMessage());
            }
        }

        if (apps.isEmpty()) {
            Toast.makeText(this, "Empty recent apps.", Toast.LENGTH_LONG)
                    .show();
            return;
        }

        PopupMenu popupMenu = new PopupMenu(buttonsBarView.getContext(), buttonsBarView.findViewById(R.id.btn_recent));
        DOUtil.setForceShowIconOnPopupMenu(popupMenu);

        Menu menu = popupMenu.getMenu();
        menu.add("Recent Apps").setEnabled(false).setIcon(null);

        for (String[] app : apps) {
            String packageName = app[0];
            Drawable icon = null;
            try {
                icon = getPackageManager().getApplicationIcon(packageName);
            } catch (Exception ignored) {
            }
            menu.add(app[1]).setIcon(icon)
                    .setOnMenuItemClickListener((item) -> {
                Log.i(TAG, "[recent] launch" + packageName);
                Intent launcher = mPackageManager.getLaunchIntentForPackage(packageName);
                startActivity(launcher);

                return true;
            });
        }

        popupMenu.show();
    }

    private void showOverlayOrientation() {
        PopupMenu popupMenu = new PopupMenu(buttonsBarView.getContext(), buttonsBarView.findViewById(R.id.btn_move));
        Menu menu = popupMenu.getMenu();
        menu.add("Orientation").setEnabled(false);

        SharedPreferences pref = LocalPreferences.getDeviceAdminPreferences(this);
        boolean isHorizontal = pref.getInt(LocalPreferences.KIOSK_MODE_OLVIEW_ORIENTATION, 0) == 0;

        menu.add("Horizontal")
                .setCheckable(true)
                .setChecked(isHorizontal)
                .setOnMenuItemClickListener((item) -> {
            buttonsBarView.setOrientation(LinearLayout.HORIZONTAL);
            pref.edit()
                    .putInt(LocalPreferences.KIOSK_MODE_OLVIEW_ORIENTATION, 0)
                    .apply();
            return true;
        });

        menu.add("Vertical")
                .setCheckable(true)
                .setChecked(!isHorizontal)
                .setOnMenuItemClickListener((item) -> {
                    buttonsBarView.setOrientation(LinearLayout.VERTICAL);
                    pref.edit()
                            .putInt(LocalPreferences.KIOSK_MODE_OLVIEW_ORIENTATION, 1)
                            .apply();
                    return true;
                });

        popupMenu.show();
    }

    private void showActiveNotifications() {
        StatusBarNotification[] notifications = getActiveNotifications() /*null*/;
        if (notifications == null) {
            Log.i(TAG, "getActiveNotifications is null");
            return;
        }

        if (notifications.length == 0) {
            Log.i(TAG, "Empty getActiveNotifications");
            return;
        }

        Log.i(TAG, "Active notifications");

        /*PopupMenu popupMenu = new PopupMenu(buttonsBarView.getContext(), buttonsBarView.findViewById(R.id.btn_notifications));
        DOUtil.setForceShowIconOnPopupMenu(popupMenu);

        Menu menu = popupMenu.getMenu();
        menu.add("Recent Notifications").setEnabled(false).setIcon(null);

        Drawable defaultIcon = getResources().getDrawable(android.R.drawable.sym_def_app_icon);
        for (StatusBarNotification sbn : notifications) {
            String packageName = sbn.getPackageName();
            Drawable icon = null;
            try {
                icon = getPackageManager().getApplicationIcon(packageName);
            } catch (Exception ignored) {
            }

            Notification notification =  sbn.getNotification();
            String title = notification.extras.getString(Notification.EXTRA_TITLE);
            //Log.i(TAG, "pkg=" + packageName + ": " + title);
            //Log.i(TAG, notification.toString() + "remoteViews=" + notification.contentView.getLayoutId());

            menu.add(title)
                    .setIcon(icon == null ? defaultIcon : icon)
                    .setOnMenuItemClickListener(item -> {
                        Log.i(TAG, "[notify-onclick]");
                        return true;
            });
        }

        popupMenu.show();*/


        LinearLayout notificationsLayout = notificationsView.findViewById(R.id.nt_list_layout);
        notificationsLayout.removeAllViews();

        for (StatusBarNotification sbn : notifications) {
            String packageName = sbn.getPackageName();
            Notification notification =  sbn.getNotification();

            Log.i(TAG, "===============================================");
            /*notification.toString()
            Log.i(TAG, "pkg: " + packageName);
            Log.i(TAG, "priority: " + (new String[] {"PRIORITY_LOW", "PRIORITY_MIN", "PRIORITY_DEFAULT", "PRIORITY_HIGH", "PRIORITY_MAX"})[notification.priority + 2]);
            Log.i(TAG, "when: " + new SimpleDateFormat("hh:mm a", Locale.US).format(new Date(notification.when)));
            if (notification.contentIntent != null) {
                Log.i(TAG, "contentIntent on touch");
                Log.i(TAG, "" + notification.contentIntent);
            }
            if (notification.actions != null) {
                Log.i(TAG, "actions: " + notification.actions.length);
                for (Notification.Action action : notification.actions) {
                    Log.i(TAG, "action[" + action.title + "] " + action.actionIntent);
                }
            }*/
            Log.i(TAG, notification.toString());
            Log.i(TAG, "===============================================");

            //LinearLayout itemLayout = (LinearLayout) layoutInflater.inflate(R.layout.item_notification_layout, null);

            SwipeLayout itemLayout = (SwipeLayout) layoutInflater.inflate(R.layout.item_notification_layout_v2, null);
            itemLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
            itemLayout.addDrag(SwipeLayout.DragEdge.Left, itemLayout.findViewById(R.id.nt_right_wrapper));
            itemLayout.setRightSwipeEnabled(false);
            itemLayout.addSwipeListener(new SimpleSwipeListener() {

                @Override
                public void onStartOpen(SwipeLayout layout) {
                    Log.i(TAG, "[onStartOpen]");
                    blockClicked1 = true;
                }

                @Override
                public void onOpen(SwipeLayout layout) {
                    Log.i(TAG, "[onOpen]");
                }

                @Override
                public void onStartClose(SwipeLayout layout) {
                    Log.i(TAG, "[onStartClose]");
                    blockClicked1 = true;
                }

                @Override
                public void onClose(SwipeLayout layout) {
                    Log.i(TAG, "[onClose]");
                }

                @Override
                public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                    Log.i(TAG, "[onHandRelease]");
                }

                @Override
                public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                    Log.i(TAG, "[onHandRelease]");
                }

            });

            //Small icon
            Drawable smallIcon = null;
            try {
                smallIcon = getPackageManager().getApplicationIcon(packageName);
            } catch (Exception ignored) {
            }
            if (smallIcon != null) {
                ((ImageView) itemLayout.findViewById(R.id.nt_small_icon)).setImageDrawable(smallIcon);
            }

            //App Name
            String appName;
            try {
                ApplicationInfo applicationInfo = getPackageManager().getApplicationInfo(packageName, 0);
                appName = getPackageManager().getApplicationLabel(applicationInfo).toString().trim();
            } catch (Exception ignored) {
                appName = "";
            }

            appName = appName.isEmpty() ? packageName : appName;
            TextView appNameView = itemLayout.findViewById(R.id.nt_app_name);
            appNameView.setText(appName);

            //[Content View]
            showContentView(notification, itemLayout);

            //[Large Content View]
            if (notification.bigContentView != null) {
                Log.i(TAG, "[content-view] has large content view");
            }

            //[Main intent]
            if (notification.contentIntent != null) {
                itemLayout.setOnClickListener(v -> {
                    if (blockClicked1) {
                        Log.i(TAG, "[contentIntent] no onClick");
                        blockClicked1 = false;
                        return;
                    }

                    Log.i(TAG, "[contentIntent] onClick");
                    try {
                        notification.contentIntent.send();

                        //Hide notifications view
                        notificationsView.setVisibility(View.GONE);

                        //Show static buttons
                        if (buttonsBarView != null) {
                            buttonsBarView.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "contentIntent ex: " + e.getMessage());
                        Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }

            //[Actions]
            View actionsLayout = itemLayout.findViewById(R.id.nt_actions_layout);
            actionsLayout.setVisibility(View.GONE);
            if (notification.actions != null && notification.actions.length > 0) {
                //actionsLayout.setVisibility(View.GONE);
                int[] buttonsIds = {R.id.nt_action1, R.id.nt_action2, R.id.nt_action3};
                Button[] actionButtons = new Button[buttonsIds.length];
                for (int x = 0; x < buttonsIds.length; x++) {
                    Button actionButton = itemLayout.findViewById(buttonsIds[x]);
                    actionButton.setVisibility(View.GONE);
                    actionButtons[x] = actionButton;
                }
                int index = 0;
                do {
                    Notification.Action action = notification.actions[index];
                    Button actionButton = actionButtons[index];

                    actionButton.setText(action.title);
                    if (action.actionIntent != null) {
                        actionButton.setOnClickListener(v -> {
                            Log.i(TAG, "[action] onClick " + action.title);
                            try {
                                action.actionIntent.send();

                                //Hide notifications view
                                notificationsView.setVisibility(View.GONE);

                                //Show static buttons
                                if (buttonsBarView != null) {
                                    buttonsBarView.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "actionIntent ex: " + e.getMessage());
                                Toast.makeText(this, "Error action: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    actionButton.setVisibility(View.VISIBLE);

                    index++;
                }  while (index < 3 && index < notification.actions.length);

                if (notification.actions.length == 1) {
                    Button actionButton = actionButtons[0];
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) actionButton.getLayoutParams();
                    params.width = 0;
                    params.weight = 1.0f;
                }

            } else {
                //actionsLayout.setVisibility(View.GONE);
            }

            //Collapse
            View headerLayout = itemLayout.findViewById(R.id.nt_header_layout);
            ImageView collapseButton = itemLayout.findViewById(R.id.nt_action_collapse);
            if (notification.actions != null && notification.actions.length > 0) {
                collapseButton.setImageDrawable(collapsedDrawable);

                headerLayout.setClickable(true);
                headerLayout.setFocusable(true);
                headerLayout.setOnClickListener(v -> {
                    Log.i(TAG, "[collapse] onClick");

                    if (collapseButton.getDrawable().equals(collapsedDrawable)) {
                        collapseButton.setImageDrawable(expandedDrawable);
                        actionsLayout.setVisibility(View.VISIBLE);

                        if (notification.bigContentView != null) {
                            showCustomContentView(itemLayout, notification.bigContentView);
                        }
                    } else {
                        collapseButton.setImageDrawable(collapsedDrawable);
                        actionsLayout.setVisibility(View.GONE);

                        showContentView(notification, itemLayout);
                    }
                });
            } else {
                collapseButton.setVisibility(View.GONE);
            }

            //Swipe actions
            //[close]
            itemLayout.findViewById(R.id.nt_action_close).setOnClickListener(v -> {
                Log.i(TAG, "[swipe-action] onClick close");
                if (notification.deleteIntent != null) {
                    Log.i(TAG, "[deleteIntent] ");
                    try {
                        //notification.deleteIntent.send();
                        cancelNotification(sbn.getKey());
                    } catch (Exception e) {
                        Log.e(TAG, "deleteIntent ex: " + e.getMessage());
                        Toast.makeText(this, "Error swipe-action: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }

                notificationsLayout.removeView(itemLayout);
            });

            notificationsLayout.addView(itemLayout);
        }

        //Show notification view
        notificationsView.setVisibility(View.VISIBLE);

        //Hide static buttons
        if (buttonsBarView != null) {
            buttonsBarView.setVisibility(View.GONE);
        }
    }

    private void showContentView(Notification notification, SwipeLayout itemLayout) {
        if (notification.contentView != null) {
            Log.i(TAG, "[content-view] custom");

            showCustomContentView(itemLayout, notification.contentView);
        } else {
            Log.i(TAG, "[content-view] default");
            showDefaultContentView(notification, itemLayout);
        }
    }

    private void showDefaultContentView(Notification notification, SwipeLayout itemLayout) {
        LinearLayout contentViewLayout = itemLayout.findViewById(R.id.nt_content_view);
        contentViewLayout.removeAllViewsInLayout();

        Bundle extras = notification.extras;
        CharSequence title = extras.getCharSequence(Notification.EXTRA_TITLE, "");
        //title =  (title == null) ? "" : title.trim();

        CharSequence text1 = extras.getCharSequence(Notification.EXTRA_TEXT, "");
        //text1 = (text1 == null) ? "" : text1.trim();

        CharSequence text2 = extras.getCharSequence(Notification.EXTRA_SUB_TEXT, "");
        //text2 = (text2 == null) ? "" : text2.trim();

        CharSequence info = extras.getCharSequence(Notification.EXTRA_INFO_TEXT, "");

        View contentView = layoutInflater.inflate(R.layout.item_notification_content_v2, null);
        TextView titleView = contentView.findViewById(R.id.nt_title);
        TextView text1View = contentView.findViewById(R.id.nt_text1);

        titleView.setText((title + (info.toString().isEmpty() ? "" : ( " ~ " + info))));
        text1View.setText((text1 + (text2.toString().isEmpty() ? "" : (": " + text2))));

        if (title.toString().isEmpty()) {
            titleView.setVisibility(View.GONE);
        }

        if (text1.toString().isEmpty()) {
            text1View.setVisibility(View.GONE);
        }

        //Large icon
        Drawable  largeIcon = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Icon icon2 = notification.getLargeIcon();
            if (icon2 != null) {
                largeIcon = icon2.loadDrawable(this);
            }
        }
        ((ImageView) contentView.findViewById(R.id.nt_large_icon)).setImageDrawable(largeIcon);

        //add
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        contentViewLayout.addView(contentView, params);
    }

    private void showCustomContentView(View itemLayout, RemoteViews contentView) {
        LinearLayout contentViewLayout = itemLayout.findViewById(R.id.nt_content_view);
        contentViewLayout.removeAllViewsInLayout();

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        View customView = contentView.apply(this, contentViewLayout);

        contentViewLayout.addView(customView, params);
    }

    //Impl - NotificationListenerService

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        Log.i(TAG, "onNotificationPosted pkg=" + sbn.getPackageName());
        Notification notification = sbn.getNotification();
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        if (blockOnNotificationRemoved) {
            return;
        }

        Log.i(TAG, "onNotificationRemoved pkg=" + sbn.getPackageName());
    }

}
