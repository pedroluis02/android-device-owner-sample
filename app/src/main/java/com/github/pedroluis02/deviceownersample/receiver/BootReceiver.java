package com.github.pedroluis02.deviceownersample.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.github.pedroluis02.deviceownersample.AppsKioskActivity;
import com.github.pedroluis02.deviceownersample.preferences.LocalPreferences;

public class BootReceiver extends BroadcastReceiver {

    private final String TAG = "BootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive action=" + intent.getAction());

        SharedPreferences preferences = LocalPreferences.getDeviceAdminPreferences(context);
        if (preferences.getBoolean(LocalPreferences.KIOSK_MODE_ENABLED, false)) {
            Log.i(TAG, "onReceive start AppsKioskActivity");
            context.startActivity(new Intent(context, AppsKioskActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

}