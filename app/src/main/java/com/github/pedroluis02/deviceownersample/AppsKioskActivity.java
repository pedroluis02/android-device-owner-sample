package com.github.pedroluis02.deviceownersample;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.RestrictionsManager;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.UserManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.github.pedroluis02.deviceownersample.preferences.LocalPreferences;
import com.github.pedroluis02.deviceownersample.receiver.DeviceOwnerReceiver;
import com.github.pedroluis02.deviceownersample.service.DOSystemOverlayService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AppsKioskActivity extends Activity {

    private final String TAG = "AppsKioskActivity";

    private GridView appsLayout;

    private ComponentName mAdminComponentName;
    private DevicePolicyManager mDevicePolicyManager;
    private PackageManager mPackageManager;

    /* chrome restriction blacklist*/
    private RestrictionsManager mRestrictionsManager;
    /*----------------------------------------*/

    private static final String Battery_PLUGGED_ANY = Integer.toString(
            BatteryManager.BATTERY_PLUGGED_AC |
                    BatteryManager.BATTERY_PLUGGED_USB |
                    BatteryManager.BATTERY_PLUGGED_WIRELESS);

    private static final String DONT_STAY_ON = "0";

    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_apps_kiosk);

        preferences = LocalPreferences.getDeviceAdminPreferences(this);

        mAdminComponentName = DeviceOwnerReceiver.getComponentName(this);
        mDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        mPackageManager = getPackageManager();
        setDefaultCosuPolicies(true);

        appsLayout = findViewById(R.id.layout_km_apps);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.canDrawOverlays(this)) {
            //startService(new Intent(this, FloatingWidgetService.class));
        }

        findViewById(R.id.action_exit).setOnClickListener((view1) -> {
            Log.i(TAG, "onclick EXIT");
            if (isInLockTaskMode()) {
                Log.i(TAG, "stopLockTask");

                setDefaultCosuPolicies(false);
                stopLockTask();

                preferences.edit()
                        .putBoolean(LocalPreferences.KIOSK_MODE_ENABLED, false)
                        .apply();

                Toast.makeText(this, "Kiosk mode deactivated!", Toast.LENGTH_LONG).show();

                //stop FloatingWidgetService
                //stopService(new Intent(this, FloatingWidgetService.class));
                startService(new Intent(this, DOSystemOverlayService.class)
                        .setAction(DOSystemOverlayService.ACTION_HIDE_MAIN_BAR));
            }
        });

        findViewById(R.id.action_recents).setOnClickListener(v -> {
            Log.i(TAG, "onclick RECENT");
            showRecentApps();
        });

        findViewById(R.id.action_whitelistall).setOnClickListener(v -> {
            Log.i(TAG, "onclick white list all");
            whiteListAll();
        });

        Log.i(TAG, "start buttons bar");
        startService(new Intent(AppsKioskActivity.this, DOSystemOverlayService.class)
                .setAction(DOSystemOverlayService.ACTION_SHOW_MAIN_BAR));
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.i(TAG, "onStart");

        setBlackWhiteList();


        // start lock task mode if it's not already active
        if (!isInLockTaskMode()) {
            Log.i(TAG, "startLockTask");
            startLockTask();

            preferences.edit()
                    .putBoolean(LocalPreferences.KIOSK_MODE_ENABLED, true)
                    .apply();

            Toast.makeText(this, "Kiosk mode activated!, tap on EXIT to finish.", Toast.LENGTH_LONG).show();
        }
    }

    private boolean setBlackWhiteList() {
        String packageName = "com.android.chrome";

        // Set up the restrictions bundle
        Bundle restrictionsBundle = new Bundle();
        restrictionsBundle.putString("URLBlacklist", "[\"*\"]");
        restrictionsBundle.putString("URLWhitelist", "[\"google.com\"]");
        mDevicePolicyManager.setApplicationRestrictions(mAdminComponentName, packageName, restrictionsBundle);
        return true;
    }

    private boolean whiteListAll() {
        String packageName = "com.android.chrome";

        // Set up the restrictions bundle
        Bundle restrictionsBundle = new Bundle();
        restrictionsBundle.putString("URLWhitelist", "[\"*\"]");
        mDevicePolicyManager.setApplicationRestrictions(mAdminComponentName, packageName, restrictionsBundle);
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");

        // stop system overlay
        //Log.i(TAG, "stop system overlay");
        //stopService(new Intent(this, DOSystemOverlayService.class));

        // load apps
        Log.i(TAG, "load kiosk apps");
        appsLayout.setAdapter(new KioskAppsAdapter(appsLayout.getContext(),
                preferences.getStringSet(LocalPreferences.KIOSK_MODE_APPS, new HashSet<>())));

        mRestrictionsManager = (RestrictionsManager) getSystemService(Context.RESTRICTIONS_SERVICE);

    }

    private void getRunningAppProcesses() {
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List <ActivityManager.RunningAppProcessInfo> list = activityManager.getRunningAppProcesses();

        try {
            for (ActivityManager.RunningAppProcessInfo info : list) {
                String packageName = Arrays.toString(info.pkgList);
                Log.i(TAG, "[running] " + info.processName + ": " + packageName);

            }
        } catch (Exception e) {
            Log.e(TAG, "[running:error] " + e.getMessage());
        }
    }

    private void showRecentApps() {
        Set <String> recentPackages = preferences.getStringSet(LocalPreferences.KIOSK_MODE_RECENT_PACKAGES, new HashSet<>());

        List <String> packages = new ArrayList<>();
        List <String> labels = new ArrayList<>();
        for (String packageName : recentPackages) {
            try {
                ApplicationInfo info = mPackageManager.getApplicationInfo(packageName, 0);
                labels.add(mPackageManager.getApplicationLabel(info).toString());

                packages.add(packageName);
            } catch (Exception e) {
                Log.i(TAG, "[recent:error] " + e.getMessage());
            }
        }

        if (packages.isEmpty()) {
            Toast.makeText(this, "Empty recent apps.", Toast.LENGTH_LONG)
                    .show();
            return;
        }

        String[] items = new String[labels.size()];
        labels.toArray(items);

        new AlertDialog.Builder(this)
                .setTitle("Recent Apps")
                .setItems(items, (dialog, index) -> {
                    String packageName = packages.get(index);
                    Log.i(TAG, "[recent] launch" + packageName);
                    Intent launcher = mPackageManager.getLaunchIntentForPackage(packageName);
                    startActivity(launcher);

                    // start system overlay
                    //Log.i(TAG, "start system overlay");
                    //startService(new Intent(AppsKioskActivity.this, DOSystemOverlayService.class));
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

    private boolean isInLockTaskMode() {
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        // ActivityManager.getLockTaskModeState api is not available in pre-M.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (!am.isInLockTaskMode()) {
                return false;
            }
        } else {
            if (am.getLockTaskModeState() == ActivityManager.LOCK_TASK_MODE_NONE) {
                return false;
            }
        }

        return true;
    }

    private void setDefaultCosuPolicies(boolean active) {
        // set user restrictions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            setUserRestriction(UserManager.DISALLOW_SAFE_BOOT, active);
        }
        setUserRestriction(UserManager.DISALLOW_FACTORY_RESET, active);
        setUserRestriction(UserManager.DISALLOW_ADD_USER, active);
        setUserRestriction(UserManager.DISALLOW_MOUNT_PHYSICAL_MEDIA, active);
        setUserRestriction(UserManager.DISALLOW_ADJUST_VOLUME, active);

        // disable keyguard and status bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mDevicePolicyManager.setKeyguardDisabled(mAdminComponentName, active);
            mDevicePolicyManager.setStatusBarDisabled(mAdminComponentName, active);
        }

        // enable STAY_ON_WHILE_PLUGGED_IN
        /*enableStayOnWhilePluggedIn(active);*/

        // set System Update policy
        /*if (active){
            mDevicePolicyManager.setSystemUpdatePolicy(mAdminComponentName,
                    SystemUpdatePolicy.createWindowedInstallPolicy(60, 120));
        } else {
            DevicePolicyManager.setSystemUpdatePolicy(mAdminComponentName, null);
        }*/

        // set this Activity as a lock task package
        Set <String> list = preferences.getStringSet(LocalPreferences.KIOSK_MODE_APPS, new HashSet<>());
        String[] packages = new String[list.size() + 1];

        int x = 0;
        packages[x] = getPackageName();
        for (String packageName: list) {
            x++;
            packages[x] = packageName;
        }
        mDevicePolicyManager.setLockTaskPackages(mAdminComponentName, active ? packages : new String[]{});

        if (active) {
            // set dedicated device activity as home intent receiver so that it is started
            // on reboot
            IntentFilter intentFilter = new IntentFilter(Intent.ACTION_MAIN);
            intentFilter.addCategory(Intent.CATEGORY_HOME);
            intentFilter.addCategory(Intent.CATEGORY_DEFAULT);

            mDevicePolicyManager.addPersistentPreferredActivity(mAdminComponentName, intentFilter,
                    new ComponentName(getPackageName(), AppsKioskActivity.class.getName()));
        } else {
            mDevicePolicyManager.clearPackagePersistentPreferredActivities(mAdminComponentName, getPackageName());
        }
    }

    private void setUserRestriction(String restriction, boolean disallow) {
        if (disallow) {
            mDevicePolicyManager.addUserRestriction(mAdminComponentName, restriction);
        } else {
            mDevicePolicyManager.clearUserRestriction(mAdminComponentName, restriction);
        }
    }

    private void enableStayOnWhilePluggedIn(boolean enabled) {
        if (enabled) {
            mDevicePolicyManager.setGlobalSetting(
                    mAdminComponentName,
                    Settings.Global.STAY_ON_WHILE_PLUGGED_IN,
                    Battery_PLUGGED_ANY);
        } else {
            mDevicePolicyManager.setGlobalSetting(
                    mAdminComponentName,
                    Settings.Global.STAY_ON_WHILE_PLUGGED_IN, DONT_STAY_ON);
        }
    }

    private static String[] convertToArray(Set <String> collection) {
        String[] items = new String[collection.size()];
        return collection.toArray(items);
    }

    private class KioskAppsAdapter extends ArrayAdapter <String> {

        private KioskAppsAdapter(Context context, Set <String> apps) {
            super(context, R.layout.layout_app_view, R.id.app_label, convertToArray(apps));
        }

        @Override
        @NonNull
        public View getView(int position, View convertView, ViewGroup parent) {
            String packageName = getItem(position);

            View appView;
            if (convertView == null) {
                appView = getLayoutInflater().inflate(R.layout.layout_app_view, null);
                appView.setOnClickListener(v -> {
                    // start main activity of package
                    Log.i(TAG, "[get-view] launch pkg=" + packageName);
                    Intent launcher = mPackageManager.getLaunchIntentForPackage(packageName);
                    startActivity(launcher);

                    // add app to history
                    Log.i(TAG, "add to history: " + packageName);
                    Set <String> currentRA = preferences.getStringSet(LocalPreferences.KIOSK_MODE_RECENT_PACKAGES, new HashSet<>());
                    currentRA.add(packageName);
                    preferences.edit()
                            .putStringSet(LocalPreferences.KIOSK_MODE_RECENT_PACKAGES, currentRA)
                            .apply();

                    // start system overlay
                    //Log.i(TAG, "start system overlay");
                    //startService(new Intent(AppsKioskActivity.this, DOSystemOverlayService.class));
                });
            } else {
                appView = convertView;
            }

            try {
                ApplicationInfo appInfo = getPackageManager().getApplicationInfo(packageName, 0);

                ImageView appIcon = appView.findViewById(R.id.app_icon);
                appIcon.setImageDrawable(appInfo.loadIcon(mPackageManager));

                TextView appLabel = appView.findViewById(R.id.app_label);
                appLabel.setText(appInfo.loadLabel(mPackageManager));
            } catch (Exception e) {
                Log.e(TAG, "[load] " + e.getMessage());
            }

            return appView;
        }
    }

}
